<?php

declare(strict_types = 1);

namespace Drupal\lb_inspector\Batch;

use Drupal\batch\Batch\EnumeratedBatchBase;

/**
 * Performs inspection.
 */
class InspectionBatch extends EnumeratedBatchBase {

  /**
   * {@inheritdoc}
   */
  public function processItem(mixed $item, array &$context) {
    // TODO: Implement processItem() method.
  }

  protected function getItems(): array {
    // TODO: Implement getItems() method.
  }

}
