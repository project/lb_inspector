<?php

declare(strict_types = 1);

namespace Drupal\lb_inspector\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Layout Builder Inspector routes.
 */
class LbInspectorController extends ControllerBase {

  /**
   * Displays inspection results.
   */
  public function __invoke(): array {

    $build['content'] = [
      '#plain_text' => $this->t('TODO: display inspection results.'),
    ];

    return $build;
  }

}
