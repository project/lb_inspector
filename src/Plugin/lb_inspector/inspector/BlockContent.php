<?php

declare(strict_types = 1);

namespace Drupal\lb_inspector\Plugin\LbInspector;

use Drupal\lb_inspector\Inspector\LbInspectorPluginBase;

/**
 * Plugin implementation of the lb_inspector.
 *
 * @LbInspector(
 *   id = "block_content",
 *   label = @Translation("Block Content"),
 *   description = @Translation("Inspect block content in layout builder content.")
 * )
 */
class BlockContent extends LbInspectorPluginBase {

}
