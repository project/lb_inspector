<?php

declare(strict_types = 1);

namespace Drupal\lb_inspector\Form;

use Drupal\awareness\Entity\EntityTypeBundleInfoAwareTrait;
use Drupal\awareness\Layout\LayoutPluginManagerAwareTrait;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\lb_inspector\Batch\InspectionBatch;
use Drupal\lb_inspector\Service\LbInspectorInspectorAwareTrait;

/**
 * Provides a Layout Builder Inspector form.
 */
class LbInspectorForm extends FormBase {

  use EntityTypeBundleInfoAwareTrait;
  use LayoutPluginManagerAwareTrait;
  use LbInspectorInspectorAwareTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'lb_inspector_inspect';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $lb_entity_types = $this->getLbInspector()->getLayoutBuilderEntityTypes();
    $entity_type_options = array_map(function ($type) {
      return $type->getLabel();
    }, $lb_entity_types);

    $entity_type_default = NULL;
    if (count($entity_type_options) == 1) {
      $keys = array_keys($entity_type_options);
      $entity_type_default = reset($keys);
    }

    $content_section_id = Html::getUniqueId($this->getFormId() . '_content');

    $form['content'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Layout'),
      '#id' => $content_section_id,
    ];

    $form['content']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#options' => $entity_type_options,
      '#default_value' => $entity_type_default,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [static::class, 'ajaxContent'],
        'wrapper' => $content_section_id,
      ],
    ];

    if (!$selected_entity_type = $form_state->getValue('entity_type')) {
      $selected_entity_type = $entity_type_default;
    }

    if ($selected_entity_type) {
      $field_options = $this->getLbInspector()
        ->getLayoutBuilderEntityFields($selected_entity_type);
      $field_options = array_combine($field_options, $field_options);

      $field_default = NULL;
      if (count($field_options) == 1) {
        $keys = array_keys($field_options);
        $field_default = reset($keys);
      }

      $form['content']['field'] = [
        '#type' => 'select',
        '#title' => $this->t('Field'),
        '#description' => $this->t('Choose which layout builder field to inspect.'),
        '#options' => $field_options,
        '#required' => TRUE,
        '#default_value' => $field_default,
      ];

      $bundle_options = array_map(function ($bundle) {
        return $bundle['label'];
      }, $this->getEntityTypeBundleInfo()->getBundleInfo($selected_entity_type));
      asort($bundle_options);

      if (count($bundle_options) > 1) {
        $form['content']['bundles'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Bundle'),
          '#description' => $this->t('Restrict inspection to only these bundles. Selecting no bundles will inspect all bundles.'),
          '#options' => $bundle_options,
        ];
      }
    }

    $layouts = $this->getLayoutPluginManager()->getDefinitions();
    $layout_options = array_keys($layouts);
    asort($layout_options);

    $layout_section_id = Html::getUniqueId($this->getFormId() . '_layout');

    $form['layouts'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Layouts'),
      '#id' => $layout_section_id,
    ];

    if (!$selected_layouts = $form_state->getValue('layouts')) {
      $selected_layouts = $entity_type_default;
    }

    $form['layouts']['layouts'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Layouts'),
      '#description' => $this->t('Restrict inspection to only these layouts. Selecting no layouts will inspect all layouts.'),
      '#options' => $layout_options,
      '#multiple' => TRUE,
      '#ajax' => [
        'callback' => [static::class, 'ajaxLayouts'],
        'wrapper' => $layout_section_id,
      ],
    ];


    $region_options = [];
    foreach ($layouts as $layout_id => $layout) {
      if (!$selected_layouts || in_array($layout_id, array_keys($layouts))) {
        $region_options = array_merge($region_options, array_keys($layout->getRegions()));
      }
    }
    $region_options = array_unique($region_options);
    asort($region_options);

    $form['layouts']['regions'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Regions'),
      '#description' => $this->t('Restrict inspection to only these layout regions. Selecting no regions will inspect all regions.'),
      '#options' => $region_options,
      '#multiple' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Inspect'),
      ],
    ];

    return $form;
  }

  /**
   * Ajax callback for handling updates in the content section.
   */
  public static function ajaxContent(array $form, FormStateInterface $form_state) {
    return $form['content'];
  }

  /**
   * Ajax callback for handling updates in the layouts section.
   */
  public static function ajaxLayouts(array $form, FormStateInterface $form_state) {
    return $form['layouts'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity_type_id = $form_state->getValue('entity_type');
    $field = $form_state->getValue('field');
    $bundles = array_keys($form_state->getValue('bundles') ?: []);

    $batch = new InspectionBatch($this->getLbInspector()->queryEntities($entity_type_id, $field, $bundles));
    batch_set($batch->toArray());
  }

}
