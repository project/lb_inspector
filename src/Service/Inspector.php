<?php

declare(strict_types = 1);

namespace Drupal\lb_inspector\Service;

use Drupal\awareness\Entity\EntityFieldManagerAwareTrait;
use Drupal\awareness\Entity\EntityTypeManagerAwareTrait;

/**
 * Layout builder inspector.
 */
class Inspector {

  use EntityFieldManagerAwareTrait;
  use EntityTypeManagerAwareTrait;

  /**
   * Get entity types with layout builder fields.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   The entity types with layout builder fields.
   */
  public function getLayoutBuilderEntityTypes(): array {
    $fields = $this->getEntityFieldManager()
      ->getFieldMapByFieldType('layout_section');
    return array_intersect_key($this->getEntityTypeManager()->getDefinitions(), $fields);
  }

  /**
   * Get layout builder fields for an entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return array
   *   Field names of the layout builder fields on the entity type.
   */
  public function getLayoutBuilderEntityFields(string $entity_type_id): array {
    $fields = $this->getEntityFieldManager()
      ->getFieldMapByFieldType('layout_section');
    return array_keys($fields[$entity_type_id] ?? []);
  }

  /**
   * Query entities to inspect.
   *
   * @return array
   *   IDs of the entities.
   */
  public function queryEntities(string $entity_type_id, string $field, array $bundles = [], int $limit = NULL): array {
    // Query entities with the field.
    $query = $this->getEntityQuery($entity_type_id)
      ->exists($field);

    // Restrict to bundles.
    if ($bundles) {
      $query->condition($this->getEntityTypeManager()->getDefinition($entity_type_id)->getKey('bundle'), $bundles, 'IN');
    }

    // Limit.
    if ($limit !== NULL)  {
      $query->range(0, $limit);
    }

    return $query->accessCheck(FALSE)->execute();
  }

}
