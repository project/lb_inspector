<?php declare(strict_types = 1);

namespace Drupal\lb_inspector\Service;

/**
 * Trait for classes that utilize the lb_inspector.inspector service.
 */
trait LbInspectorInspectorAwareTrait {

  /**
   * Get the lb_inspector.inspector service.
   *
   * @return \Drupal\lb_inspector\Service\Inspector
   *   The lb_inspector.inspector service.
   */
  protected function getLbInspector(): Inspector {
    return \Drupal::service('lb_inspector.inspector');
  }

}
