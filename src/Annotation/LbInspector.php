<?php declare(strict_types = 1);

namespace Drupal\lb_inspector\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines lb_inspector annotation object.
 *
 * @Annotation
 */
final class LbInspector extends Plugin {

  /**
   * The plugin ID.
   */
  public readonly string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $description;

}
