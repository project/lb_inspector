<?php

declare(strict_types = 1);

namespace Drupal\lb_inspector\Inspector;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\lb_inspector\Annotation\LbInspector;

/**
 * LbInspector plugin manager.
 */
class LbInspectorPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/lb_inspector/inspector', $namespaces, $module_handler, LbInspectorInterface::class, LbInspector::class);
    $this->alterInfo('lb_inspector_info');
    $this->setCacheBackend($cache_backend, 'lb_inspector_plugins');
  }

}
