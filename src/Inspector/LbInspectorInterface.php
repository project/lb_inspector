<?php

declare(strict_types = 1);

namespace Drupal\lb_inspector\Inspector;

/**
 * Interface for lb_inspector plugins.
 */
interface LbInspectorInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

}
