<?php

declare(strict_types = 1);

namespace Drupal\lb_inspector\Inspector;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for lb_inspector plugins.
 */
abstract class LbInspectorPluginBase extends PluginBase implements LbInspectorInterface {

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
